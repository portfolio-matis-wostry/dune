<?php

use function PHPSTORM_META\type;

/* [1] - - - - - -  
API (Application Programming Interface)est un programme permettant à deux applications distinctes de 
communiquer entre elles et d’échanger des données. Cela évite notamment de recréer et redévelopper entièrement une
application pour y ajouter ses informations. Par exemple, elle est là pour faire le lien entre des données 
déjà existantes et un programme indépendant.

CURL, ou Client URL, est une bibliothèque logicielle et une ligne de commande utilisée pour transférer des 
données avec des URL. En soit effectuer des requêtes HTTP en PHP
Ici une session cURL est créée, c'est comme une série d'instruction cURL regroupées pour d'accomplir certaines tâches*/
$api = curl_init();

/* [2] - - - - - -  
Les options cURL sont propres à la bibliothèque cURL pour personnaliser le comportement des données
lors de l'exécution de requêtes HTTP

Ici, 
CURLOPT_URL :
Cette option définit l'URL à laquelle la requête cURL sera envoyée. C'est l'URL de l'API TMDB pour obtenir les crédits
du film avec l'ID 438631, donc Dune

CURLOPT_RETURNTRANSFER :
Indique à cURL de retourner les données en châine de caractère au lieu de les afficher. les donées sont stockées
dans la variable $data

CURLOPT_TIMEOUT définit le temps d'attente maximal pour la requête en secondes.
*/
curl_setopt_array($api, [
    CURLOPT_URL => "https://api.themoviedb.org/3/movie/438631/credits?api_key=4f89ff5921c8f5e2ff47867e6a9fc19f",
    CURLOPT_RETURNTRANSFER => true, // Retourner les données dans $data
    CURLOPT_TIMEOUT        => 1, // Temps d'attente maxi en secondes
]);
// ATTENTION, il se peut que vous ayez à résoudre un problème de certificat avec Wamp

/* [3] - - - - - - 
La reqête cURL est éxécutées et stoque les données récupérées dans la variable $data
*/
$data = curl_exec($api);

/* [4] - - - - - -
 Gestion des erreurs :
 S'il y a une erreur alors data est définie sur null
 */
if ($data === false || curl_getinfo($api, CURLINFO_HTTP_CODE !== 200)) {
    $data = null;
}

/* [5] - - - - - -
 Affichage des données au format JSON (JavaScript Object Notation)
 C' est un format léger d'échange de données qui est facile à lire et à écrire pour les humains, et facile à 
 analyser et à générer pour les machines.
 */
echo $data;

curl_close($api);
?>