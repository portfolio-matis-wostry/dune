# Dune

Ce site web est le résultat d'un projet de cours où nous devions apprendre plusieurs aspects non vus auparavant, comme l'utilisation d'API, jQuery, Ajax, etc.

Le site n'est pas encore responsive et n'est adapté que pour les ordinateurs.
